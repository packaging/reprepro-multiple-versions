FROM debian:bookworm as builder

COPY bookworm/reprepro_*_amd64.deb /tmp/

RUN --mount=type=cache,id=build-cache,target=/build-cache,sharing=locked \
 cp /tmp/reprepro_*_amd64.deb /build-cache/

FROM debian:bookworm-slim

# dummy copy to depend on builder
COPY --from=builder /etc/issue.net /etc/issue.net

RUN --mount=type=cache,id=build-cache,target=/build-cache,sharing=locked \
 apt-get update \
 && apt-get -y install \
  gnupg2 \
  curl \
  /build-cache/reprepro_*_amd64.deb \
 && apt-get clean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
