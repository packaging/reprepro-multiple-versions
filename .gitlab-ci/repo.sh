#!/bin/bash

set -e

apt-get -qq update
apt-get -qq -y install reprepro gnupg2
gpg --import "${SIGNING_KEY_2022_08_28}"
test -f "${CI_PROJECT_DIR}/.repo/gpg.key" || gpg --export --armor "${SIGNING_KEY_ID}" >"${CI_PROJECT_DIR}/.repo/gpg.key"
for codename in jammy bookworm; do
    sed -i 's,##SIGNING_KEY_ID##,'"${SIGNING_KEY_ID}"',' ".repo/${codename}/conf/distributions"
    find "${CI_PROJECT_DIR}/${codename}" -type f -name "*.deb" -exec \
        reprepro -b "${CI_PROJECT_DIR}/.repo/${codename}" includedeb "${codename}" {} \+
done
