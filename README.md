# [Reprepro with multiple versions support](https://github.com/ionos-enterprise/reprepro) apt packages

## Use Docker Image

You can use the pre-built docker image `registry.gitlab.com/packaging/reprepro-multiple-versions:latest`

## Use Packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-reprepro-multiple-versions.asc https://packaging.gitlab.io/reprepro-multiple-versions/gpg.key
```

### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/reprepro-multiple-versions bookworm main" | sudo tee /etc/apt/sources.list.d/morph027-reprepro-multiple-versions.list
```
